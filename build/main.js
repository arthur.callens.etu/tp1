"use strict";

/*let name = `regina`;
let url = `images/${ name }.jpg`;

console.log(url);

let html = `<article class="pizzaThumbnail">
                <a href="${url}">
                    <img src="${url}"/>
                    <section>${name}</section>
                </a>
            </article>`;

console.log(html);*/
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
var html2 = "";
/*data.sort(function(a,b) {
    if(a.price_small = b.price_small)
        return a.price_large-b.price_large;
    else
        return a.price_small-b.price_small;
})*/

var result = data.filter(function (obj) {
  var i = 0;
  if (obj.name.includes('i')) i++;
  if (i <= 2) return 1;else return -1;
});
result.forEach(function (i) {
  var url2 = "images/".concat(i.name, ".jpg");
  html2 += "<article class=\"pizzaThumbnail\">\n                    <a href=\"".concat(url2, "\">\n                        <img src=\"").concat(i.image, "\" />\n                        <section>\n                            <h4>").concat(i.name, "</h4>\n                            <ul>\n                                <li>Prix petit format : ").concat(i.price_small.toFixed(2), "\u20AC</li>\n                                <li>Prix grand format : ").concat(i.price_large.toFixed(2), "\u20AC</li>\n                            </ul>\n                        </section>\n                    </a>\n                </article>");
  console.log(html2);
  document.querySelector('.pageContent').innerHTML = html2;
});
//# sourceMappingURL=main.js.map